package ru.kharlamova.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
