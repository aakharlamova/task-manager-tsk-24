package ru.kharlamova.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.kharlamova.tm.comparator.ComparatorByCreated;
import ru.kharlamova.tm.comparator.ComparatorByDateStart;
import ru.kharlamova.tm.comparator.ComparatorByName;
import ru.kharlamova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by start date", ComparatorByDateStart.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    @NotNull
    private String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
