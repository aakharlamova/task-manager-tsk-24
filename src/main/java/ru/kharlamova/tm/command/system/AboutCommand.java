package ru.kharlamova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Anastasia Kharlamova");
        System.out.println("E-MAIL: aakharlamova@tsconsulting.com");
    }

}
