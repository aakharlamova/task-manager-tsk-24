package ru.kharlamova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.model.AbstractEntity;

import java.util.*;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final List<E> result = new ArrayList<>();
        for (@NotNull final E entity : entities) {
            if (entity.equals(entity.getId())) result.add(entity);
        }
        return entities;
    }

    @NotNull
    @Override
    public E add(@NotNull final E entity) {
        entities.add(entity);
        return entity;
    }

    @Nullable
    @Override
    public Optional<E> findById(@NotNull final String id) {
        return entities.stream()
                .filter(entity -> id.equals(entity.getId()))
                .findFirst();
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @NotNull final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public void remove(@Nullable final E entity) {
        entities.remove(entity);
    }

}
