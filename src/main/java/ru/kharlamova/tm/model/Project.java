package ru.kharlamova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.entity.IWBS;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractBusinessEntity implements IWBS {

    public Project(@Nullable final String name) {
        this.name = name;
    }

}
