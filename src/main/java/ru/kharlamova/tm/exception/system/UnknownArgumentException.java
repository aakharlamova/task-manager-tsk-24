package ru.kharlamova.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.kharlamova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(@NotNull final String argument) {
        super("Error! Unknown argument ``" + argument + "``.");
    }

}
