package ru.kharlamova.tm.exception.empty;

import ru.kharlamova.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! User ID is empty.");
    }

}
