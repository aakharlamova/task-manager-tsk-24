package ru.kharlamova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.enumerated.Role;
import ru.kharlamova.tm.model.User;

import java.util.Optional;

public interface IUserService {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    Optional<User> setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    Optional<User> findById(@NotNull String id);

    @Nullable
    Optional<User> findByLogin(@Nullable String login);

    @Nullable
    Optional<User> findByEmail(@Nullable String email);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    Optional<User> lockUserByLogin(@Nullable String login);

    @NotNull
    Optional<User> unlockUserByLogin(@Nullable String login);

    @Nullable
    Optional<User> updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

}
