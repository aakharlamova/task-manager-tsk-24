package ru.kharlamova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<String> getCommandArgs();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    AbstractCommand getCommandByArg(String arg);

    @NotNull
    AbstractCommand getCommandByName(String name);

    void add(@Nullable AbstractCommand command);

}
